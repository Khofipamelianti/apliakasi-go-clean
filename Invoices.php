<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Invoices extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->library('email');
    }
    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'Halaman Data Invoice';
        $this->load->model('Menu_model', 'menu');
        $this->load->model('Pesan_model', 'pesan');
        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();
        $this->load->library('pagination');
        if ($this->input->post('submit')) {
            $data['keyword'] = $this->input->post('keyword');
        } else {
            $data['keyword'] = null;
        }
        $this->db->like('id_pemesanan', $data['keyword']);
        $this->db->from('pemesanan');
        $config['base_url']   = 'http://localhost/DADI-RESIK/Invoices/index';
        $config['total_rows'] = $this->db->count_all_results();
        $data['totalrows'] = $config['total_rows'];
        $config['per_page'] = 8;
        $config['full_tag_open'] = '<nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data['start'] = $this->uri->segment(3);
        $data['pesan'] = $this->pesan->tampilinvoice($config['per_page'], $data['start'], $data['keyword']);
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/sidebar', $data);
        $this->load->view('admin/template/topbar', $data);
        $this->load->view('admin/invoice', $data);
        $this->load->view('admin/template/footer');
    }

    public function editinvoice($id){
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'Edit Pemesanan';
        $this->load->model('Menu_model', 'menu');
        $this->load->model('Pesan_model', 'pesan');
        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();
        $where = array('id_brg' => $id);
        $data['invoice'] =  $this->pesan->edit_invoice($id);
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/sidebar', $data);
        $this->load->view('admin/template/topbar', $data);
        $this->load->view('admin/edit_invoice', $data);
        $this->load->view('admin/template/footer');
    }

    public function updateinvoice()
    {
        $waktu = $this->input->post('waktu');
        $tanggal = $this->input->post('tanggal');
        $data['title'] = 'Invoice';
        $this->load->model('Menu_model', 'menu');
        $this->load->model('Pesan_model', 'pesan');
        $id = $this->input->post('id_pemesanan');
        $this->db->select('*');
        $this->db->from('pemesanan');
        $this->db->join('user', 'user.id = pemesanan.id');
        $this->db->join('tb_barang', 'tb_barang.id_brg = pemesanan.id_brg');
        $this->db->where('id_pemesanan', $id);
        $query = $this->db->get();
        $result = $query->row_array();
        $hp = $result['hp'];
        $id_pemesanan = $result['id_pemesanan'];
        $nama = $result['nama'];
        $pesanan = $result['nama_brg'];
        $harga = $result['harga'];
        $tanggal = $result['tanggal'];
        $waktu = $result['waktu'];
        $email = $this->input->post('email');
        $data = array(
            'tanggal' => $tanggal,
            'waktu'  => $waktu,
            'status' => 2
        );
        $where = array(
            'id_pemesanan' => $id
        );
        $this->pesan->update_invoice($where, $data, 'pemesanan');
        if($data['status']==2){
                $to_email = $email;
                $subject = "Konfirmasi Pesanan Dadi Resik ";
                $message = "Dari : Usaha Dadi Resik
Yogyakarta,Bantul (0812-123-5432)
Pesanan dengan rincian :
Id Pesanan : ".$id."
Nama Pemesan : ".$nama."
No.Hp : ".$hp."
Pesanan : ".$pesanan."
Harga : ".$harga."
Tanggal : ".$tanggal."
Waktu : ".$waktu." WIB
Pesanan diatas telah berhasil TERKONFIRMASI.
Silahkan simpan bukti kirimin ini dan serahkan kepada petugas Dadi Resik sebagai bukti pesanan.";
                if (!empty($to_email)) {
                    $config = [
                        'mailtype' => 'text',
                        'charset' => 'iso-8859-1',
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://smtp.googlemail.com',
                        'smtp_user' => 'dadiresikyogyakarta@gmail.com',
                        'smtp_pass' => 'dadiresikyk',
                        'smtp_port' => 465
                    ];
                    $this->load->library('email', $config);
                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");
                    $this->email->from($config['smtp_user']);
                    $this->email->to($to_email);
                    $this->email->subject($subject);
                    $this->email->message($message);
                    if ($this->email->send()) {
                         redirect('invoices/editinvoice/'.$id);
                    } else {
                        show_error($this->email->print_debugger());
                    }
                }
            }
              redirect('invoices/editinvoice/'.$id);
    }
    public function hapusinvoice($id)
    {
        $this->db->where('id_pemesanan', $id);
        $this->db->delete('pemesanan');
        redirect('invoices/index');
    }
    public function chatwa($id){
        $this->db->select('*');
        $this->db->from('pemesanan');
        $this->db->join('user', 'user.id = pemesanan.id');
        $this->db->join('tb_barang', 'tb_barang.id_brg = pemesanan.id_brg');
        $this->db->where('id_pemesanan', $id);
        $query = $this->db->get();
        $result = $query->row_array();
        $hp = $result['hp'];
        $id_pemesanan = $result['id_pemesanan'];
        $nama = $result['nama'];
        $pesanan = $result['nama_brg'];
        $harga = $result['harga'];
        $tanggal = $result['tanggal'];
        $waktu = $result['waktu'];
        header("location:https://api.whatsapp.com/send?phone=$hp&text=Dari : Usaha Dadi Resik%20%0DAlamat:%20Yogyakarta,Bantul%20%0DNo.Hp:%20(+62) 
        123-5432.%20%0D------------------------------------------%20%0DPesanan dengan rincian :%20%0DId Pesanan:%20$id_pemesanan%20%0D
        Nama Pemesan:%20$nama%20%0DPesanan:%20$pesanan%20%0DHarga:%20$harga%20%0DTanggal:%20$tanggal%20%0DWaktu:%20$waktu
        %20%0D-------------------------------------------%20%0D......");
    }
    public function chatwakonfirm($id){
        $this->db->select('*');
        $this->db->from('pemesanan');
        $this->db->join('user', 'user.id = pemesanan.id');
        $this->db->join('tb_barang', 'tb_barang.id_brg = pemesanan.id_brg');
        $this->db->where('id_pemesanan', $id);
        $query = $this->db->get();
        $result = $query->row_array();
        $hp = $result['hp'];
        $id_pemesanan = $result['id_pemesanan'];
        $nama = $result['nama'];
        $pesanan = $result['nama_brg'];
        $harga = $result['harga'];
        $tanggal = $result['tanggal'];
        $waktu = $result['waktu'];
        header("location:https://api.whatsapp.com/send?phone=$hp&text=Hai,%20kami%20dari%20Dadi%20Resik%20ingin%20menginformasikan%20bahwa%20pesanan%20anda%20dengan
        %20Id%20Pesanan%20$id_pemesanan%20telah%20berhasil%20DIKONFIRMASI.%20Silahkan%20Cek%20Bukti%20Pemesanan%20melalui%20email%20anda%20atau%20dapat%20melalui
        %20website%20pada%20akun%20anda%20.Terima Kasih !!");
    }
    public function laporan(){
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'Halaman Lihat Laporan Pemesanan';
        $this->load->model('Menu_model', 'menu');
        $this->load->model('Pesan_model', 'pesan');
        $this->load->model('Lapor');
        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();
        $this->form_validation->set_rules('tanggalmulai', 'Periode Tanggal', 'required');
        $this->form_validation->set_rules('tanggalakhir', 'Periode Tanggal', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/template/header', $data);
            $this->load->view('admin/template/sidebar', $data);
            $this->load->view('admin/template/topbar', $data);
            $this->load->view('admin/laporan', $data);
            $this->load->view('admin/template/footer');
        } else {
            $input = $this->input->post(null, true);
            $tanggalmulai = $input['tanggalmulai'];
            $tanggalakhir = $input['tanggalakhir'];

            $query = $this->Lapor->getLaporan(null, ['mulai' => $tanggalmulai, 'akhir' => $tanggalakhir]);
                $this->_cetak($query, $tanggalmulai, $tanggalakhir);
        }
    }
    private function _cetak($data, $tanggalmulai, $tanggalakhir)
    {
        $this->load->library('CustomPDF');

        $pdf = new FPDF();
        $pdf->AddPage('P', 'Letter');
        $pdf->SetFont('Times', 'B', 16);
        $pdf->Cell(190, 7, 'Laporan Penjualan Dadi Resik', 0, 1, 'C');
        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(190, 4, 'Tanggal : ' . $tanggalmulai . ' - ' . $tanggalakhir, 0, 1, 'C');
        $pdf->Ln(10);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(10, 7, 'No.', 1, 0, 'C');
        $pdf->Cell(30, 7, 'Tgl Penjualan', 1, 0, 'C');
        $pdf->Cell(45, 7, 'Nama', 1, 0, 'C');
        $pdf->Cell(35, 7, 'No.Telp', 1, 0, 'C');
        $pdf->Cell(50, 7, 'Nama Jasa/Barang', 1, 0, 'C');
        $pdf->Cell(20, 7, 'Harga', 1, 0, 'C');
        $pdf->Ln();
        $no = 1;
        foreach ($data as $d) {
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(10, 7, $no++ . '.', 1, 0, 'C');
            $pdf->Cell(30, 7, $d['tanggal'], 1, 0, 'C');
            $pdf->Cell(45, 7, $d['nama'], 1, 0, 'C');
            $pdf->Cell(35, 7, $d['hp'], 1, 0, 'L');
            $pdf->Cell(50, 7, $d['nama_brg'], 1, 0, 'L');
            $pdf->Cell(20, 7, $d['harga'], 1, 0, 'C');
            $pdf->Ln();
        }
        $file_name = 'Laporan ' . $tanggalmulai . '-' . $tanggalakhir;
        $pdf->Output('I', $file_name);
    }
}
