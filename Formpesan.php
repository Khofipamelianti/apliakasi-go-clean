<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Formpesan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelPesan');
        $this->load->model('m_invoice');
    }
    public function index($id_brg)
    {
        $data['title'] = ' Form Pemesanan';
        $data['invoice'] = $this->m_invoice->get_no_invoice();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Pesan_model', 'pesan');
        $this->load->model('Menu_model', 'menu');
        $data['MenuUser'] = $this->menu->getMenuUser();
        $data['barang'] = $this->db->get_where('tb_barang', ['id_brg' => $id_brg])->result_array();
        $this->load->view('user/template2/header', $data);
        $this->load->view('user/template2/sidebar');
        $this->load->view('user/template2/topbar', $data);
        $this->load->view('user/formpesan', $data);
    }

    public function tambah_pesan()
    {
        $nama = $this->input->post('nama');
        $id_pemesanan = $this->input->post('id_pemesanan');
        $id_brg = $this->input->post('id_brg');
        $id = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');
        $waktu = $this->input->post('waktu');
        $metode = $this->input->post('recorded');
        $data = array(
            'nama' => $nama,
            'id_pemesanan' => $id_pemesanan,
            'id' => $id,
            'id_brg' => $id_brg,
            'tanggal' => $tanggal,
            'waktu' => $waktu,
            'metode_bayar'    => $metode,
            'status' => 'Pending'
        );
        $this->db->insert('pemesanan', $data);
        $this->db->select('*');
        $this->db->from('pemesanan');
        $this->db->order_by('id_pemesanan', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->row_array();
        $insert = $result['id_pemesanan'];

        redirect('FormPesan/struk_bayar/' . $insert);
    }

    public function struk_bayar($insert)
    {
        $data['title'] = ' Struk Pemesanan';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Pesan_model', 'pesan');
        $this->load->model('Menu_model', 'menu');
        $data['MenuUser'] = $this->menu->getMenuUser();
        $data['pemesanan'] = $this->ModelPesan->struk_bayar($insert);
        $this->load->view('public/template/head', $data);
        $this->load->view('public/template/menu_head');
        $this->load->view('public/home/struk_bayar', $data);
        $this->load->view('public/template/isipesanan');
        $this->load->view('public/template/end_content');
        $this->load->view('public/template/footer');
    }
    public function cek()
    {
        $data['title'] = 'Cek Pemesanan';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');
        $data['MenuUser'] = $this->menu->getMenuUser();
        $this->load->view('public/template/head', $data);
        $this->load->view('public/template/menu_head');
        $this->load->view('public/home/cek');
        $this->load->view('public/template/isipesanan');
        $this->load->view('public/template/end_content');
        $this->load->view('public/template/footer');
    }
    public function cancel($id)
    {
        $this->db->where('id_pemesanan', $id);
        $this->db->delete('pemesanan');
        $this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert">Menu Telah Dihapus</div>');
        redirect('formpesan/index/');
    }
    public function checkout()
    {
        $data['title'] = ' Form Pemesanan';
        $data['invoice'] = $this->m_invoice->get_no_invoice();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Pesan_model', 'pesan');
        $this->load->model('Menu_model', 'menu');
        $id = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');
        $waktu = $this->input->post('waktu');
        $nohp = $this->input->post('no_hp');
        $alamat = $this->input->post('alamat');
        $metode_bayar = $this->input->post('metode_bayar');
         $nohp = str_replace(" ","",$nohp);
         $nohp = str_replace("(","",$nohp);
         $nohp = str_replace(")","",$nohp);
         $nohp = str_replace(".","",$nohp);
         if(!preg_match('/[^+0-9]/',trim($nohp)))
         {
         if(substr(trim($nohp), 0, 3)=='+62')
         {
         $hp = trim($nohp);
         }
         elseif(substr(trim($nohp), 0, 1)=='0')
         {
         $hp = '+62'.substr(trim($nohp), 1);
         }
         }
         else
         {
         $hp = 'Format no hp yang dimasukkan tidak lengkap atau salah!';
         }
        $update = array(
            'alamat' => $alamat,
            'hp'     => $hp

        );
        $this->db->where('id', $id);
        $this->db->update('user', $update);
        $data = [
            'id_pemesanan' => $this->input->post('id_pemesanan'),
            'id' => $this->input->post('id'),
            'id_brg' => $this->input->post('id_brg'),
            'harga' => $this->input->post('harga'),
            'tanggal' => $tanggal,
            'waktu' => $waktu,
            'status' => 0,
            'metode_bayar' => $metode_bayar,
        ];
        $this->db->insert('pemesanan', $data);
        $this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">Barang Telah berhasil ditambah</div>');
        redirect('konfirmasi/index/'.$this->input->post('id_pemesanan'));
    }
}
