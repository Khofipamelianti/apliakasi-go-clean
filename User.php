<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->library('form_validation');
    }
    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'Dadi Resik';
        $this->load->model('Menu_model', 'menu');
        $data['MenuUser'] = $this->menu->getMenuUser();
        $this->load->view('user/template2/header', $data);
        $this->load->view('user/template2/sidebar');
        $this->load->view('user/template2/topbar', $data);
        $this->load->view('user/home');
        $this->load->view('user/template2/footer');
    }
    public function editprofile()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'Dadi Resik';
        $this->load->model('Menu_model', 'menu');

        $data['MenuUser'] = $this->menu->getMenuUser();
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/sidebar', $data);
        $this->load->view('admin/template/topbar', $data);
        $this->load->view('public/home/edit_profil', $data);
        $this->load->view('admin/template/footer');
    }
    public function editprofile2()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'Dadi Resik';
        $this->load->model('Menu_model', 'menu');
        $data['MenuUser'] = $this->menu->getMenuUser();
        $this->load->view('user/template2/header', $data);
        $this->load->view('user/template2/sidebar', $data);
        $this->load->view('user/template2/topbar', $data);
        $this->load->view('user/edit_profiluser', $data);
        $this->load->view('user/template2/footer');
    }
    public function update()
    {
        $this->load->model('Menu_model', 'menu');
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $email = $this->input->post('email');
        $gambar = $_FILES['gambar']['name'];
        if ($gambar = '') {
        } else {
            $config['upload_path'] = './assets/img/gambarProfil/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('gambar')) {
                echo "Gambar gagal diupload";
            } else {
                $gambar = $this->upload->data('file_name');
            }
        }
        $data = array(
            'nama' => $nama,
            'alamat' => $alamat,
            'hp' => $no_hp,
            'email' => $email
        );
        if ($this->input->post('password') != null) {
            $data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
        }
        if ($gambar != null) {
            $data['gambar'] = $gambar;
        }
        $where = array(
            'id' => $id
        );
        $this->menu->update_data($where, $data, 'user');
        redirect('auth/logout_akun');
    }
    public function updateuser()
    {
        $this->load->model('Menu_model', 'menu');
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $email = $this->input->post('email');
        $gambar = $_FILES['gambar']['name'];
        if ($gambar = '') {
        } else {
            $config['upload_path'] = './assets/img/gambarProfil/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('gambar')) {
                echo "Gambar gagal diupload";
            } else {
                $gambar = $this->upload->data('file_name');
            }
        }
        $data = array(
            'nama' => $nama,
            'alamat' => $alamat,
            'hp' => $no_hp,
            'email' => $email
        );
        if ($this->input->post('password') != null) {
            $data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
        }
        if ($gambar != null) {
            $data['gambar'] = $gambar;
        }
        $where = array(
            'id' => $id
        );
        $this->menu->update_data($where, $data, 'user');
        redirect('auth/logout_akun');
    }
}
