<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }
    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'Halaman Data Barang';
        $this->load->model('Menu_model', 'menu');
        $this->load->model('Pesan_model', 'pesan');
        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();
        $this->load->library('pagination');
        if ($this->input->post('submit')) {
            $data['keyword'] = $this->input->post('keyword');
        } else {
            $data['keyword'] = null;
        }
        $this->db->like('nama_brg', $data['keyword']);
        $this->db->from('tb_barang');
        $config['base_url']   = 'http://localhost/DADI-RESIK/barang/index';
        $config['total_rows'] = $this->db->count_all_results();
        $data['totalrows'] = $config['total_rows'];
        $config['per_page'] = 5;
        $config['full_tag_open'] = '<nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';

        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data['start'] = $this->uri->segment(3);
        $data['barang'] = $this->pesan->tampilBarang($config['per_page'], $data['start'], $data['keyword']);
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/sidebar', $data);
        $this->load->view('admin/template/topbar', $data);
        $this->load->view('admin/barang', $data);
        $this->load->view('admin/template/footer');
    }
    public function tambahbarang()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'Tambah Barang';
        $this->load->model('Menu_model', 'menu');
        $this->load->model('Pesan_model', 'pesan');
        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['barang'] = $this->pesan->tampilBarang(null,null,null);

        $this->form_validation->set_rules('nama_brg', 'Nama Barang', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
        $this->form_validation->set_rules('kategori', 'Kategori', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size']     = '2048';
        $config['upload_path'] = './assets/img/gambarProduk/';
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $old_image = $data['tb_barang']['gambar'];
            if ($old_image != 'default.jpg') {
                unlink(FCPATH . 'assets/img/gambarProduk/' . $old_image);
            }
            $new_image = $this->upload->data('file_name');
        } else {
        }
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/template/header', $data);
            $this->load->view('admin/template/sidebar', $data);
            $this->load->view('admin/template/topbar', $data);
            $this->load->view('admin/tambahbarang', $data);
            $this->load->view('admin/template/footer');
        } else {
            $data = [
                'nama_brg' => $this->input->post('nama_brg'),
                'keterangan' => $this->input->post('keterangan'),
                'kategori' => $this->input->post('kategori'),
                'harga' => $this->input->post('harga'),
                'gambar_barang' => $new_image,
            ];
            $this->db->insert('tb_barang', $data);
            $this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">Barang Telah berhasil ditambah</div>');
            redirect('barang');
        }
    }
    public function editbarang($id){
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'Edit Barang';
        $this->load->model('Menu_model', 'menu');
        $this->load->model('Pesan_model', 'pesan');
        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();
        $where = array('id_brg' => $id);
        $data['barang'] = $this->pesan->edit_barang($where, 'tb_barang')->result();
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/sidebar', $data);
        $this->load->view('admin/template/topbar', $data);
        $this->load->view('admin/edit_barang', $data);
        $this->load->view('admin/template/footer');
    }
    public function updatebarang()
    {
        $data['title'] = 'Barang';
        $this->load->model('Menu_model', 'menu');
        $this->load->model('Pesan_model', 'pesan');
        $id = $this->input->post('id_brg');
        $nama_brg = $this->input->post('nama_brg');
        $keterangan = $this->input->post('keterangan');
        $kategori = $this->input->post('kategori');
        $harga = $this->input->post('harga');
        $gambar = $_FILES['gambar']['name'];
        if ($gambar = '') {
        } else {
            $config['upload_path'] = './assets/img/gambarProduk/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('gambar')) {
                echo "Gambar gagal diupload";
            } else {
                $gambar = $this->upload->data('file_name');
            }
        }
        $data = array(
            'nama_brg' => $nama_brg,
            'keterangan' => $keterangan,
            'kategori' => $kategori,
            'harga' => $harga
        );
        if ($gambar != null) {
            $data['gambar_barang'] = $gambar;
        }
        $where = array(
            'id_brg' => $id
        );
        $this->pesan->update_data($where, $data, 'tb_barang');
        redirect('barang/index');
    }
    public function hapus($id)
    {
        $this->db->where('id_brg', $id);
        $this->db->delete('tb_barang');
        redirect('barang/index');
    }
}
